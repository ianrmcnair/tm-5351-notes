import { computed } from 'vue'
import characters from './data/characters'
import concepts from './data/concepts'
import groups from './data/groups'
import locations from './data/locations'
import objects from './data/objects'
import types from './data/types'

const notes = computed(() => [
  ...characters.value,
  ...concepts.value,
  ...groups.value,
  ...locations.value,
  ...objects.value,
])

export default notes;

const normalizeName = str => str.trim().toLowerCase().replace(/^the /i, '');

export const getNote = name => {
  const note = notes.value.filter(note => normalizeName(note.name) === normalizeName(name) || note.alternateNames?.map(normalizeName).includes(normalizeName(name)));

  const notFound = [
    'RECORD NOT FOUND'
  ];

  if (note.length > 0) {
    if (note.length > 1) console.error(`Multiple notes found for "${name}".`);
    return {
      ...note[0],
      notes: note[0].notes && note[0].notes.length > 0 ? note[0].notes : notFound
    };
  }
  console.error(`Note not found for "${name}".`);
  return {
    name: 'NOT FOUND',
    type: types.value.INVALID,
    notes: notFound
  };
};
export { types, normalizeName };