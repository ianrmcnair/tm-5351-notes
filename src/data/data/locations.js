import { ref } from 'vue'
import types from './types'

export default ref([
  {
    name: "Overture Site",
    notes: [
      "A [[Protomethean]] research facility near [[Balderkhole]] operated by, until [[Willow Wisp|recently]], [[The Adamantine Arrow]]. It was built around [[BULWARK]].",
      "I wake up here at the start of the [[time loop]]. There six stasis chambers for [[cyborgs]]: [[me]], [[DF-9276]], [[HF-2093]], [[EM-4718]], and two unknown."
    ]
  },
  {
    name: "Sultana",
    notes: [
      "[[Lost Leaf]] lived here when he was taken by the [[Adamantine Arrow]]."
    ]
  },
  {
    name: "Balderkhole",
  },
  {
    name: "Leonia",
    notes: [
      "A city that controls the gates to the [[Tears of Mul'dronak]]."
    ]
  },
  {
    name: "Taurent",
    notes: [
      "Birthplace of [[Yohar Tau]]."
    ]
  },
  {
    name: "Free Bridge",
    notes: [
      "[[Rah'li]] has family here."
    ]
  },
  {
    name: "The Omnicanonical Brightlane",
    notes: [
      "A plane containing infinite array of alternate realms."
    ]
  },
  {
    name: "The Great Walls",
  },
  {
    name: "Ortoss",
  },
  {
    name: "The Tears of Mul'dronak",
    notes: [
      "Three lakes connected to the sea via the [[Leonia]] sea gates.",
      "Home of [[Mul'dronak]], a giant eel. Also home to lots of *very* tasty fish. Too bad he'll attack anyone threatening his fish."
    ]
  },
  {
    name: "The Soul Forest",
  },
  {
    name: "Arcadia",
  },
  {
    name: "Pandemonium",
  },
  {
    name: "The Twisted Glade",
    notes: [
      "A forest corrupted by [[antedark crystals]] near [[Balderkhole]]. Where we first encountered [[antedark]].",
      "This forest was once protected by a dryad, but the dryad was killed."
    ]
  },
  {
    name: "Frosithaj",
  },
  {
    name: "Shrines of Zaltus",
    notes: [
      "There are 6 Shrines of [[Zaltus]] across the realm:",
      "One in the Bay of Vesta (visited)",
      "One under [[Free Bridge]]",
      "One near the Great Northern Island",
      "One near the [[Cat's Claw Isles]]",
      "One in the Living Abyss",
      "One in the Frigid Depths",
      "We may need to visit these shrines to locate the crown of Zaltus, which [[BULWARK]] requires for his [[primary mission]].",
      "The first shrine contained an elaborate mural depicting a lich ([[Garenseth]]?) corrupting a dragon with [[Antedark crystals]]. The dragon's mate fled under the sea and became an electric eel ([[Azaloq]] or [[Mul'dronak]]?)."
    ]
  },
  {
    name: "The Sulfur Spires",
  },
  {
    name: "The Feywilds",
  },
  {
    name: "The Cat's Claw Isles",
    notes: [
      "Homeland of the Tabaxi - and of [[Lost Leaf]] and [[Willow Wisp]]."
    ]
  },
  {
    name: "The Ghostlands",
  },
  {
    name: "Sairinsburg"
  }
].map(location => ({ type: types.value.LOCATION, ...location })))