import { ref } from 'vue'
import types from './types'
import colors from '../colors'

export default ref([
  {
    name: "Antedark Crystals",
    subTypes: [
      "Crystal"
    ],
    alternateNames: [
      "Antedark Crystal"
    ],
    notes: [
      "A corrosive crystal created by [[Antedark]].",
      "Originally entered our realm via the [[Ortoss Planar Gateway]].",
      "These crystals explode when struck with arcane energy and are harmful to the touch. The only known way to safely destroy one is to use a [[Protomethean]] mining laser.",
      "[[Garenseth]] has apparently weaponized these crystals, accelerating their growth. Once the crystals reach a critical mass, the realm experiences [[derealization]]."
    ]
  },
  {
    name: "The Verdant",
    subTypes: [
      "Vehicle"
    ],
    color: colors.EMERALD,
    notes: [
      "[[A]]'s advanced extraplanar vessel traveling the [[Omnicanonical Brightlane]], with a chronocore capable of maintaining the [[time loop]].",
      "Made out of [[emerald]].",
      "As long as the chronocore is active, any other [[time travel]] is impossible, at least according to A.",
      "While aboard the Verdant, we experience time dilation - 1 hour on the Verdant is equal to 1 minute in our normal realm."
    ]
  },
  {
    name: "The Blue Pearl",
    subTypes: [
      "Vehicle",
      "Boat"
    ],
    notes: [
      "[[Slimeaza]]'s advanced, high-speed boat.",
      "The hull has been upgraded with extremely durable weirding wood etched with the [[temporal pattern]]."
    ]
  },
  {
    name: "Tau Crystals",
    subTypes: [
      "Crystal"
    ],
    alternateNames: [
      "Tau",
      "Tau crystal",
      "Akrinite crystal",
      "Akrinite"
    ]
  },
  {
    name: "Emerald",
    subTypes: [
      "Crystal"
    ],
    color: colors.EMERALD,
    notes: [
      "Part of [[BULWARK]]'s mission is to collect emeralds.",
      "Has some kind of memory-preserving property.",
      "The primary material used to construct the [[Verdant]]."
    ]
  },
  {
    name: "Gourd",
    subTypes: [
      "Plant"
    ],
    notes: [
      "Any of the hard-shelled fruits of certain members of the gourd family, Cucurbitaceae.",
      "Many gourds are cultivated as ornamentals, decorations, or food crops, and some can be dried and used to make decorative or useful objects.",
      "[[BULWARK]] needs one that's been hoarded thrice. What the heck does that mean, and what could [[A]] want this for? I don't see the appeal, personally."
    ]
  },
  {
    name: "Ortoss Planar Gateway",
    subTypes: [
      "Device"
    ],
    notes: [
      "Located deep under the ruins of [[Ortoss]].",
      "Capable of creating portals to distant planes, such as [[Arcadia]].",
      "[[BULWARK]]'s primary mission included retrieving the gateway's focus crystal. [[A]] is using this crystal to upgrade the [[Verdant]]."
    ]
  },
  {
    name: "Slimeaza's Amulet",
    subTypes: [
      "Apparel"
    ],
    notes: [
      "A mysterious amulet that [[Slimeaza]] carries.",
      "Opens the [[Shrines of Zaltus]] and houses the spirit of [[Azaloq]]."
    ]
  },
  {
    name: "Myrkr's Scythe",
    subTypes: [
      "Weapon"
    ],
    notes: [
      "A heavily-modified farm implement and [[Myrkr]]'s primary weapon. Currently bonded to [[Rakrinoth]]'s eye."
    ]
  },
  {
    name: "The Ironspine Express",
    subTypes: [
      "Vehicle",
      "Train"
    ]
  }
].map(object => ({ type: types.value.OBJECT, ...object })))