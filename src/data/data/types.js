import { ref } from 'vue'
import colors from '../colors'

export default ref({
  CHARACTER: {
    name: "character",
    color: colors.FUR,
    label: "Person"
  },
  OBJECT: {
    name: "object",
    label: "Object"
  },
  GROUP: {
    name: "group",
    color: colors.FUR,
    label: "Group"
  },
  LOCATION: {
    name: "location",
    label: "Location"
  },
  CONCEPT: {
    name: "concept",
    label: "Concept"
  },
  INVALID: {
    name: "invalid",
    color: colors.SHIRT,
    label: "INVALID",
    nav: false
  }
});