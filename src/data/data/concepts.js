import { ref } from 'vue'
import types from './types'
import colors from '../colors'

export default ref([
  {
    name: "The Filter",
    notes: [
      "A countermeasure to the [[time loop]], created by the [[Antedark]].",
      "Trying to share knowledge of the loop both scrambles the information and results in incredible pain for both parties."
    ]
  },
  {
    name: "Time Loop",
    alternateNames: [
      "Loop"
    ],
    notes: [
      "The realm has been trapped in a time loop by [[A]], using the [[Verdant]], in order to stave off [[derealization]] by [[Antedark]].",
      "The loop lasts for 30 days, and has repeated thousands of times. At the start of the loop, [[TM|I]] wake up in my chamber at [[Overture Site]]; the loop ends when the world does.",
      "It is possible to [[In the Loop|become aware of the loop]], as I have. This can happen spontaneously, but it's also possible for a being like A make someone aware of this. Once you become aware, you remain aware through loops.",
      "However, the [[Filter]] prevents sharing of this information through ordinary means."
    ]
  },
  {
    name: "In the Loop",
    notes: [
      "Being in the loop means that you are aware of the [[time loop]]. Those in the loop retain their memories and objects from loop to loop.",
      "Some people have been in the loop for a very long time.",
      "[[The Filter]] prevents people from getting in the loop.",
      "We know of a handful of people that are confirmed in the loop:",
      "The party and [[A]], of course",
      "[[Willow Wisp]]",
      "[[DF-9276]]",
      "[[Rakrinoth]]",
      "[[VANGUARD]]",
      "[[Agustus Rockington]]",
      "[[Zail]]",
      "[[Veld]]",
      "[[FL00DCL0T]]",
      "[[Kit Caliber]]",
      "Additionally, there five known organizations manipulating the loop:",
      "[[The Guardians of the Veil]]",
      "[[The Mysterium]]",
      "[[The Silver Ladder]]",
      "[[The Free Council]]",
      "[[The Adamantine Arrow]]"
    ]
  },
  {
    name: "Derealization",
    notes: [
      "The breaking down of a realm's matter and energy. The realm returns to the [[Omnicanonical Brightlane]] as fiction, to be used to create a new realm.",
      "Bad if you happen to still be living there."
    ]
  },
  {
    name: "Temporal Pattern",
    color: colors.EMERALD,
    notes: [
      "A green fractal pattern created by [[Lost Leaf]]. Looking at it causes him and me to travel in time. To when, I'm not sure.",
      "While the [[Verdant]]'s chronocore is active, all time travel is diverted there.",
      "Appears to only cause headaches in others.",
      "The pattern appears to protect objects from being reset by the [[time loop]]. We found the pattern etched into the [[Blue Pearl]]'s storage chest, and its contents were preserved loops. Who could have put it there?"
    ],
    alternateNames: [
      "time travel"
    ],
    image: require("@/assets/temporal-pattern.jpg")
  },
  {
    name: "The Greater Crystal Network",
    notes: [
      "A technomagical network that connects [[Tau]] devices, allowing people to share information and communicate in real time across great distances."
    ]
  },
  {
    name: "Cyborgs",
    alternateNames: [
      "cyborg"
    ],
    notes: [
      "The merging of organic beings with magitechnical augmentation, created by the [[Adamantine Arrow]].",
      "I know of 4 cyborgs:",
      "[[Me]]",
      "[[DF-9276]]",
      "[[HF-2093]] (haven't met)",
      "[[EM-4718]]"
    ]
  },
  {
    name: "The Mycelial Network",
    notes: [
      "Some kind of magical mushroom network?",
      "[[MYCELIUM]] has some connection to this network, which he can use to gather information.",
      "Some of the Mycelial Network has been corrupted by [[antedark crystals]]."
    ]
  },
  {
    name: "Delvr",
    notes: [
      "A [[Tau]] app designed to connect adventurers to adventurees.",
      "They say its algorithm is designed to create optimal adventuring parties. However, according to [[Veld]], the algorithm *actually* just forms a party with [[me]], [[Myrkr]], [[Slimeaza]], and [[BULWARK]]."
    ]
  },
  {
    name: "Monetary Dynasticism",
    alternateNames: [
      "Money Cult"
    ],
    subTypes: [
      "Religion"
    ]
  },
  {
    name: "Hyperion Slayer",
    subTypes: [
      "Game"
    ]
  },
  {
    name: "The Drakonid Accords",
    subTypes: [
      "Treaty"
    ]
  }
].map(concept => ({ type: types.value.CONCEPT, ...concept })))