import { ref } from "vue";
import types from './types'
import colors from '../colors'

export default ref([
  {
    name: "Acanthus",
    alternateNames: [
      "A"
    ],
    notes: [
      "A mysterious [[Arcadia|Arcadian]] [[Fey]]. Creator of [[BULWARK]], the [[Fire Team]], and the [[Verdant]].",
      "Attempting to prevent the [[derealization]] of our realm."
    ],
    color: colors.EMERALD,
    subTypes: [
      'Fey',
      'Primarch'
    ]
  },
  {
    name: "BULWARK",
    notes: [
      "An ancient machine, part of the [[Fire Team]] created by [[A]] to combat the [[Antedark]]. Programmed with a [[primary mission]].",
      "Has a single-minded determination to carry out whatever mission he happens to be on. Doesn't seem to be particularly picky about who gives him the mission."
    ],
    image: require('@/assets/Bulwark_1.jpg'),
    subTypes: [
      'Fire Team'
    ]
  },
  {
    name: "ARBALAST",
    image: require('@/assets/arbalast.png'),
    subTypes: [
      'Fire Team'
    ],
    notes: [
      "The second of [[BULWARK'S team]] that we encountered.",
      "His antlers are some sort of communications array. When we met him, they were damaged."
    ]
  },
  {
    name: "MYCELIUM",
    image: require('@/assets/mycelium.png'),
    subTypes: [
      'Fire Team'
    ],
    notes: [
      "A mushroomy member of [[BULWARK'S team]].",
      "Has some kind of connection to the [[Mycelial Network]], which he can use to gather information at great distances."
    ]
  },
  {
    name: "VANGUARD",
    subTypes: [
      'Fire Team'
    ],
    notes: [
      "The only known member of the [[Fire Team]] I haven't met. He's been lost somewhere in the mountains south of [[Balderkhole]].",
      "[[A]] attempted to brute-force breaking the [[Filter]] on him. He's [[in the loop]], but is stuck constantly invoking the Filter. [[Repairing VANGUARD|He requires maintenance]]."
    ]
  },
  {
    name: "TREBUCHET",
    image: require('@/assets/trebuchet.png'),
    subTypes: [
      'Fire Team'
    ],
    notes: [
      "A member of the [[Fire Team]] we first encountered when he teamed up with the [[Adamantine Arrow]] in their attempt to kidnap me at the [[Ortoss Planar Gateway]]."
    ]
  },
  {
    name: "SIEGE",
    image: require('@/assets/siege.jpg'),
    subTypes: [
      'Fire Team'
    ],
    notes: [
      "An apparently simple-minded member of the [[Fire Team]]. The muscle, I suppose."
    ]
  },
  {
    name: "TM-5351",
    alternateNames: [
      "TM",
      "Me"
    ],
    notes: [
      "The funnest (probably) of the [[Adamantine Arrow]] [[cyborgs]]. Looks good in a cape.",
      "Was [[Lost Leaf]] until he wiped his memory. Now I'm me instead!"
    ],
    image: require('@/assets/tm-5351-token.png'),
    subTypes:[
      'Me',
      'Tabaxi',
      'Cyborg'
    ]
  },
  {
    name: "Lost Leaf (Leaf)",
    alternateNames: [
      "Lost Leaf",
      "Leaf"
    ],
    notes: [
      "[[Me]], sort of? I don't really remember being him. Is that still me? This is a question for the philosomancers.",
      "Left the [[Cat's Claw Isles]] at a young age and was pursued by [[Willow Wisp]].",
      "Wisp says he was a tailor, a party cat, and traveled with a theatre company for a time.",
      "He lived in [[Sultana]].",
      "At some point, he discovered the [[temporal pattern]] and attracted the attention of the [[Adamantine Arrow]], who turned him into a [[cyborg]].",
      "After working with them for some time, he decided to wipe his own memory to escape the [[time loop]]. And here I am, I suppose."
    ],
    subTypes:[
      'Tabaxi',
      'Tailor'
    ]
  },
  {
    name: "Slimeaza",
    image: require('@/assets/Slimeaza_2.jpg'),
    notes: [
      "A seafaring merfolk and captain/owner of the [[Blue Pearl]]. Has kind of a \"pirate with a heart of gold\" thing going on - very concerned with justice, morality, alignment, things like that.",
      "Something about her - perhaps her innate magical abilities - has attracted the attention of [[Garenseth]]. What does he want?",
      "Has an amulet containing the spirit of [[Azaloq]].",
      "She seems to be getting scalier as time goes on. TODO: Skin cream?"
    ],
    subTypes:[
      'Merfolk',
      'Sorcerer/Pirate'
    ]
  },
  {
    name: "Myrkr Ormr",
    alternateNames: [
      "Myrkr"
    ],
    image: require('@/assets/Myrkr_Ormr_3.jpg'),
    notes: [
      "A half-dragon/half-tiefling mercenary and [[Myrkr's Scythe|scythe enthusiast]].",
      "Son of [[Yviana Ormr]] and [[Myridious]].",
      "Poor impulse control. Has a tendency to make secret deals with shady individuals and spirits."
    ],
    subTypes: [
      'Dragonborn',
      'Tiefling',
      'Mercenary'
    ]
  },
  {
    name: "Rah'li",
    image: require('@/assets/rahli.png'),
    subTypes: [
      "Half-Orc",
      "Pointer",
      "Inland Geometrics"
    ],
    notes: [
      "The pointer assigned to us by [[Inland Geometrics]]. Her job is to write about our travels in her journal on the [[Greater Crystal Network]]. She's not supposed to interfere, but she does anyway.",
      "Over the course of [[time loop|loop 1]] we became good friends I think. She was even down for helping us betray her boss.",
      "[[BULWARK]] and [[Myrkr]] may have ruined that for this loop. We might need to [[Ditch Rah'li|ditch her]].",
      "Inland Geometrics is apparently desperate to have Rah'li follow us. They promised her extravagant rewards to convince her to keep the contract despite everything.",
      "Fan of [[FL00DCL0T]]."
    ]
  },
  {
    name: "Willow Wisp (Wisp)",
    alternateNames: [
      "Willow Wisp",
      "Wisp"
    ],
    image: require('@/assets/willow-wisp.jpg'),
    notes: [
      "A [[Klajh Miew]] warrior, whose mission was to track and bring back [[Lost Leaf]]. After seeing that he was happy, she hesitated.",
      "After witnessing the abduction of Leaf, swore an oath of vengeance against the [[Adamantine Arrow]].",
      "Died at [[Overture Site]] just before the start of the loop. She [[Cat Lord|gets better]]."
    ],
    subTypes: [
      'Tabaxi'
    ]
  },
  {
    name: "DF-9276",
    alternateNames: [
      "DF",
      "Amalia"
    ],
    notes: [
      "Another [[Adamantine Arrow]] [[cyborg]], like me. Before she was taken by the Arrow, she was an alchemist called Amalia.",
      "It's not clear how trustworthy she is. She did try to kidnap me for the Arrow, but after that failed she seemed pretty passive. Plus, she's met [[A]] now and knows the full stakes of the [[time loop]].",
      "If nothing else, we have a sort of cybernetic kinship. We're siblings, sort of. I think."
    ],
    image: require('@/assets/df-9276.png'),
    subTypes: [
      'Dragonborn',
      'Adamantine Arrow',
      'Cyborg'
    ]
  },
  {
    name: "The Cat Lord",
    subTypes: [
      'Tabaxi',
      'God'
    ],
    notes: [
      "God of felines, including the Tabaxi. According to Tabaxi legend, whenever ",
      "The [[Klajh Miew]] are sworn to his service. When they die in his service, he is able to resurrect them; this happened to [[Willow Wisp]]."
    ]
  },
  {
    name: "Azaloq",
    image: require('@/assets/azaloq.jpg'),
    subTypes: [
      'Spirit'
    ]
  },
  {
    name: "Mul'dronak",
    alternateNames: [
      "Unagi"
    ],
    notes: [
      "An eel that's so big, they named the lakes he lives in after him. He protects the fish and waters of the [[Tears of Mul'dronak]].",
      "Very dangerous, but will provide you with a very good fish if you help him out."
    ],
    subTypes: [
      'Giant Eel'
    ]
  },
  {
    name: "Garenseth",
    alternateNames: [
      "Skeletor"
    ],
    notes: [
      "Big bad skeleton dude who wants [[Slimeaza]], for reasons unknown.",
      "Has weaponized [[antedark crystals]]."
    ],
    subTypes: [
      'Lich'
    ]
  },
  {
    name: "Yohar Tau",
    image: require("@/assets/yohar-tau.jpg"),
    subTypes: [
      'Half-Elf',
      "CEO",
      "Inland Geometrics"
    ],
    notes: [
      "Discoverer of [[Tau crystals]], inventor of the [[Greater Crystal Network]], and founder of [[Inland Geometrics]].",
      "Seems very invested in tracking us via [[Rah'li]]",
      "Towards the end of the [[loop]], he goes missing after Inland Geometrics documents are dropped onto the [[Greater Crystal Network]].",
      "What's wrong with his face? I don't think he's a [[cyborg]]..."
    ]
  },
  {
    name: "Veld",
    notes: [
      "A person that [[Slimeaza]] and [[Rah'li]] met on the [[Greater Crystal Network]].",
      "Given how much he seems to know about us, there's a good chance he's [[in the loop]]. Not confirmed though.",
      "In [[loop]] 1, he convinced us to load a virus into [[Inland Geometrics]]' network."
    ]
  },
  {
    name: "FL00DCL0T",
    alternateNames:[
      "FLOODCLOT"
    ],
    notes: [
      "A musician whose concerts are combat-oriented. It's very high-concept. But fun!",
      "You get a t-shirt for \"winning\" the concert."
    ],
    subTypes: [
      'Musician'
    ]
  },
  {
    name: "Zaltus",
    subTypes: [
      'Merfolk',
      'King'
    ],
    notes: [
      "An ancient king of the merfolk.",
      "We've found some of his jewelry in the [[Shrines of Zaltus]]. [[BULWARK's primary mission]] includes collecting his crown."
    ]
  },
  {
    name: "Yviana Ormr",
    alternateNames: [
      "MILF",
      "Yviana"
    ],
    notes: [
      "Hot mom of [[Myrkr Ormr]]."
    ],
    image: require('@/assets/yviana.png'),
    subTypes: [
      'Tiefling'
    ]
  },
  {
    name: "Myridious",
    subTypes: [
      'Dragon'
    ],
    notes: [
      "[[Myrkr Ormr]]'s father.",
      "Is he still with [[Yviana]]? This could be very important."
    ]
  },
  {
    name: "Rakrinoth",
    notes: [
      "A dubious entity whose eye currently inhabits [[Myrkr's scythe]]."
    ],
    subTypes: [
      'Spirit'
    ]
  },
  {
    name: "Cyriss Mellencamp",
    alternateNames: [
      "Mr. Mellencamp"
    ],
    subTypes: [
      'Human',
      "Inland Geometrics"
    ],
    notes: [
      "A very fat man and [[Rah'li]]'s boss from [[Inland Geometrics]]. Rides around in a very comfortable-looking hover chair."
    ],
    image: require('@/assets/mellencamp.png')
  },
  {
    name: "Agustus Rockington",
    subTypes: [
      "Minotaur",
      "CEO",
      "Minosoft",
    ]
  },
  {
    name: "HF-2093",
    subTypes: [
      "Cyborg",
      "Adamantine Arrow",
      "Human (presumably)"
    ],
    notes: [
      "A \"tinkerer\" for the [[Adamantine Arrow]]."
    ]
  },
  {
    name: "EM-4718",
    subTypes: [
      "Cyborg",
      "Adamantine Arrow",
      "Elf"
    ],
    image: require('@/assets/em-4718.jpg'),
    notes: [
      "A ruthless [[cybernetic|cyborg]] sniper for the [[Adamantine Arrow]].",
      "In his former life, he was a gunsmith."
    ]
  },
  {
    name: "Zail",
    subTypes: [
      "Kobold"
    ]
  },
  {
    name: "Kira",
    subTypes: [
      "Kobold",
      "Fey"
    ]
  },
  {
    name: "Brig Shatterstone",
    subTypes: [
      "WDSC",
      "Detective-Yefreitor"
    ]
  },
  {
    name: "Reggdro Vallawick",
    subTypes: [
      "Gnome",
      "Artificer"
    ]
  },
  {
    name: "Mastigos",
    alternativeNames: [
      'M'
    ],
    subTypes: [
      'Demon',
      'Primarch'
    ]
  },
  {
    name: "Mozette",
    subTypes: [
      "Kobold"
    ],
    notes: [
      "[[Myridious]]'s receptionist."
    ]
  },
  {
    name: "Tystamogrus",
    subTypes: [
      "Dragon"
    ]
  },
  {
    name: "Furixarkkra",
    subTypes: [
      "Dragon"
    ]
  },
  {
    name: "Aldus Ramwood",
    subTypes: [
      "Human"
    ]
  },
  {
    name: "Kit Caliber",
    subTypes: [
      "Furry",
      "The Silver Ladder"
    ],
    notes: [
      "Leader of [[The Silver Ladder]].",
      "Takes drugs to keep up appearances as a werefox.",
      "Claims to have been around for thousands of loops."
    ]
  },
  {
    name: "Nandas",
    subTypes: [
      "Merfolk"
    ]
  }
].map(character => ({ ...character, type: types.value.CHARACTER })))