import { ref } from 'vue'
import types from './types'
import colors from '../colors'

export default ref([
  {
    name: "Antedark",
    notes: [
      "Antedark is an entropic force; its purpose is to recycle dying worlds back into [[The Omnicanonical Brightlane]].",
      "Antedark produces [[antedark crystals]].",
      "Some Antedark are smokelike creatures which are capable of disguising themselves as people.",
      "Long ago, Antedark creatures appeared in the [[Protomethean]] civilization, before the arrival of the crystals.",
      "Infra-Arcane sensors are capable of detecting the magic used for this disguise. Look for people with subtle magic covering their bodies.",
      "[[A]] says the Antedark has become more aggressive, attacking still-living worlds.",
      "Responsible for the [[Filter]] preventing sharing knowledge of the [[time loop]].",
      "The Protometheans believed that there exists an opposite, creative force.",
      "[[Kit Caliber]] believes that Antedark mostly impersonate people who are in the loop."
    ],
    color: colors.SHIRT
  },
  {
    name: "Protomethean",
    notes: [
      "An advanced precursor civilization. Their ruins, such as  such as the [[Ortoss Planar Gateway]] and [[The Great Walls]], can be found all over our realm.",
      "Wiped out after being overrun by [[Antedark]]."
    ]
  },
  {
    name: "Fire Team",
    alternateNames: [
      "BULWARK's Team"
    ],
    notes: [
      "A group of ancient robots created by the [[Fey]] and [[A]]:",
      "[[BULWARK]]",
      "[[ARBALAST]]",
      "[[MYCELIUM]]",
      "[[VANGUARD]]",
      "[[TREBUCHET]]",
      "[[SIEGE]]",
      "And two other unknown robots.",
      "Their [[primary mission]] is assist A in preventing the [[derealization]] of the realm."
    ]
  },
  {
    name: "The Klajh Miew",
    notes: [
      "An order of Tabaxi warriors sworn to the service of the [[Cat Lord]].",
      "Generally armed with nano-serrated weaponry.",
      "If one falls in their duties, they can be resurrected by the Cat Lord - though they have to spend some time as a house cat, first.",
      "[[Willow Wisp]] is a member."
    ]
  },
  {
    name: "Inland Geometrics",
    subTypes: [
      "Corporation"
    ],
  },
  {
    name: "The Guardians of the Veil",
    notes: [
      "An organization dedicated to strengthening the [[Filter]] and preventing more people from [[in the loop|entering the loop]].",
      "Only a few of their elite agents are allowed to remain in the loop to facilitate this.",
      "Sort of the opposite of the [[Silver Ladder]]."
    ]
  },
  {
    name: "The Mysterium",
    notes: [
      "This group uses secrets obtained using the [[time loop]] to control major corporations and governments.",
      "According to [[A]], they have largely compromised [[Inland Geometrics]]."
    ]
  },
  {
    name: "The Silver Ladder",
    notes: [
      "A group dedicated to bringing *everyone* [[in the loop|into the loop]] by bypassing the [[Filter]]. Sort of the opposite of the [[Guardians of the Veil]].",
      "They believe once everyone is in the loop, the [[loop]] will end. [[A]] says this would be bad, actually."
    ]
  },
  {
    name: "The Free Council",
    notes: [
      "A group attempting to form an independent society and government for those [[in the loop]]."
    ]
  },
  {
    name: "The Adamantine Arrow",
    alternateNames: [
      "The Arrow"
    ],
    notes: [
      "The Arrow wish to use the [[loop]] to achieve magitechnical superiority over all others. They also attempt to protect people from the danger of the [[Filter]].",
      "They also, prior to the loop, kidnapped people with rare magical knowledge such as [[Lost Leaf]] and [[Amalia]] and turned them into [[cyborgs]].",
      "[[Willow Wisp]]'s mission is to get revenge on the Arrow and sabotage them as much as she can, after she witnessed the abduction of Leaf."
    ]
  },
  {
    name: "Fey",
    notes: [
      "There are two known groups of Fey - those native to our realm, and those from [[Arcadia]].",
      "The Arcadian Fey are masters of temporal and fate magics. [[A]] is one such Fey, who constructed the [[Verdant]] and created the [[time loop]]. They also created the [[Fire Team]].",
      "First contact with Arcadia was made by the [[Protometheans]] via the [[Ortoss Planar Gateway]], thousands of years ago. The native Fey were already present in this realm at that time.",
      "The Fey of our realm live in the [[Feywilds]].",
      "It's unknown if the two groups of Fey are related - though they do share a common language."
    ]
  },
  {
    name: "Demons"
  },
  {
    name: "The Primarchs"
  },
  {
    name: "Minosoft",
    subTypes: [
      "Corporation"
    ],
    notes: [
      "An experimental weapons manufacturer run by [[Augustus Rockington]]. Terrible working conditions."
    ]
  },
  {
    name: "Wellspearian Department of Soul Crimes (WDSC)",
    alternateNames: [
      "WDSC",
      "Wellspearian Department of Soul Crimes",
      "Department of Soul Crimes",
      "Soul Cops"
    ]
  },
  {
    name: "Kira's Coven"
  },
  {
    name: "Department of Dragon Affairs",
    alternateNames: [
      "DDA"
    ]
  },
  {
    name: "The Sunbreakers"
  }
].map(group => ({ type: types.value.GROUP, ...group })))