import { ref, computed } from 'vue'
import getTimeOfDay from '../util/time-of-day';

const loopNumber = ref(2)
const timelineLength = ref(720)
const timelineStartOffset = ref(6)
const getTimelineTimeOfDay = time => (timelineLength.value + time + timelineStartOffset.value) % 24
const getTimelineDay = time => Math.floor(31 + ((time + timelineStartOffset.value) / 24))

const timeline = ref([
  {
    time: -361,
    present: true
  },
  {
    time: -451,
    longRest: true
  },
  {
    time: "The Distant Past",
    sorting: -100,
    events: [
      "[[Antedark]] infiltrates the [[Protomethean]] civilization; [[Antedark crystals]] begin to spread from the [[Ortoss Planar Gateway]].",
      "[[The Great Walls]] are constructed to contain the spread.",
      "[[A]] constructs [[BULWARK]] and the [[Fire Team]] and sends them from [[Arcadia]] to find a means of stopping the [[Antedark]].",
      "[[BULWARK]] goes dormant, and the Protometheans construct [[Overture Site]] around him.",
      "Protomethean civilization falls."
    ],
    gap: 3
  },
  {
    time: "The More Recent Past",
    sorting: -50,
    events: [
      "[[Yohar Tau]] discovers [[Tau]] crystals and invents the [[Greater Crystal Network]]. [[Inland Geometrics]] is founded.",
      "[[The Adamantine Arrow]] takes over [[Overture Site]] and studies [[BULWARK]]. They create at least 4 [[cyborgs]].",
      "[[Slimeaza]] is kidnapped and put up in a slave auction. [[Garenseth]] attempts to purchase her, but she is instead purchased by a group of druids from the [[Soul Forest]], who raise her.",
      "[[Lost Leaf]] creates the [[temporal pattern]]."
    ]
  },
  {
    time: "Outside the Timeline",
    sorting: 10000000,
    location: "The Omnicanonical Brightlane",
    events: [
      "[[A]] constructs the [[Verdant]] and begins the [[time loop]].",
      "After many loops, some people start to become aware of the [[time loop]]. Things start to change [[in the loop]].",
      "[[Antedark]] implements the [[Filter]] as a countermeasure to the [[time loop]]."
    ]
  },
  {
    time: "Just Before the Loop",
    sorting: -1,
    events: [
      "Goblins kidnap animals at the Ridgeborough Animal Sanctuary.",
      "Anndrata Pyres loses a box in Lake Murkmire containing an [[emerald]], and a nearby group of lizardmen enslaves some kobolds, including [[Zail]].",
      "Kurim Vasteer's colleagues are lost in the ruins of a [[Protomethean]] spider research facility near Balderkhole.",
      "A gnollcromancer makes gnoll zombies at an encampment near Varis Mechlin's property.",
      "[[Reggdro Vallawick]]'s soulless body turns up at a [[WDSC]] office."
    ]
  },
  {
    time: -721,
    location: "Overture Site",
    events: [
      "[[Willow Wisp]] attacks in an attempt to free [[Lost Leaf]], killing everyone inside. She dies in the attack."
    ]
  },
  {
    time: -720,
    location: "Balderkhole",
    events: [
      "[[Rah'li]], [[Myrkr Ormr]], and [[Slimeaza]] receive distress call from [[Overture Site]]."
    ]
  },
  {
    time: -720,
    location: "Overture Site",
    events: [
      "[[BULWARK]] re-activates and breaks out from containment.",
      "Hopefully, someone releases [[me]].",
      "Don't forget to pick up [[Willow Wisp]]'s body and equipment! She'll get better.",
      "BULWARK's release triggers a containment breach alarm, summoning a few familiar-looking security drones.",
      "TODO: Figure out if I can release myself, and figure out if we can disarm the alarm. Make sure every everyone else is cool around [[Rah'li]] next time."
    ]
  },
  {
    time: -720,
    location: "Sultana",
    events: [
      "[[DF]] starts the loop here at the Banded Bangle, a fancy inn."
    ]
  },
  {
    time: -718,
    location: "Taurent",
    events: [
      "An officer of the [[WDSC]] shows up at [[Reggdro Vallawick]]'s lab after being tipped off by [[Veld]]. He is promptly killed by spectral spiders."
    ]
  },
  {
    time: -700,
    location: "The Twisted Glade",
    events: [
      "Lazuli Harkenfold, a druid, is injured in the glade.",
      "According to her, the forest's dryad went missing, and shortly after [[antedark crystals]] began to appear.",
      "The dryad is killed by deathdogs, attracted by the crystals.",
      "TODO: Can we save the dryad? If the dryad was keeping the [[antedark]] at bay, they might be able to help."
    ]
  },
  {
    time: -650,
    location: "Balderkhole",
    events: [
      "Several townsfolk are infected by a deathdog plague. The deathdogs came from the nearby [[Twisted Glade]], corrupted by exposure to [[antedark crystals]]."
    ]
  },
  {
    time: -648,
    location: "Freebridge",
    events: [
      "The waters around Freebridge become infected with some kind of \"cosmic horror\".",
      "Around the same time, people start disappearing."
    ]
  },
  {
    time: -622,
    events: [
      "[[Willow Wisp]] is resurrected by the [[Cat Lord]] as a house cat. Typically she tries to find her way to us, but hopefully we've got her with us already."
    ]
  },
  {
    time: -586,
    location: "Taurent",
    events: [
      "[[FL00DCL0T]] concert."
    ]
  },
  {
    time: -584,
    events: [
      "[[Willow Wisp]] is fully restored to life."
    ]
  },
  {
    time: -574,
    location: "Balderkhole",
    events: [
      "[[Mr. Mellencamp]] is killed by the deathdog plague.",
      "If this happens, [[Rah'li]] gets threatened with reassignment."
    ]
  },
  {
    time: -575,
    location: "Myridious's Lair",
    events: [
      "[[Tystamogrus]] angrily leaves [[Myridious]]'s lair after a therapy session. [[Furixarkkra]] is shortly found dead in her lair by the [[DDA]], apparently killed by Tystamogrus."
    ]
  },
  {
    time: -537,
    location: "Sultana",
    events: [
      "[[The Sunbreaker]] Exhibition has a random opening that we can slot into."
    ]
  },
  {
    time: -510,
    location: "Sultana",
    events: [
      "[[FL00DCL0T]] concert."
    ]
  },
  {
    time: -480,
    location: "Freebridge",
    events: [
      "Freebridge Station becomes pass-through only for the [[Ironspine Express]], though the train will continue stopping there for at least 24 hours."
    ]
  },
  {
    time: -400,
    location: "Leonia",
    events: [
      "[[The Tears of Mul'dronak]] are closed due to a threat from poachers."
    ]
  },
  {
    time: -386,
    location: "The Tears of Mul'dronak",
    events: [
      "The poachers attack [[Mul'dronak]]."
    ]
  },
  {
    time: -364,
    location: "Sultana",
    events: [
      "A man with a skull for a head visits [[Ahverginus]].",
    ]
  },
  {
    time: -150,
    location: "The Omnicanonical Brightlane",
    events: [
      "In Loop 2, the [[Brightgate]] will be complete at this time."
    ]
  },
  {
    time: -12,
    location: "Free Bridge",
    events: [
      "[[Garenseth]] attacks using [[Antedark crystals]], devastating the city. He demands [[Slimeaza]] and threatens to attack [[The Soul Forest]]."
    ]
  },
  {
    time: -10,
    events: [
      "Documents are released exposing that [[Inland Geometrics]] has been suppressing knowledge of the spread of [[antedark crystals]].",
      "Inland Geometrics' stock price plummets, and [[Yohar Tau]] goes missing."
    ]
  },
  {
    time: 0,
    location: "The Soul Forest",
    gap: 5,
    events: [
      "[[Garenseth]] carries out his threat. This attack accelerates the spread of [[Antedark]], causing the [[derealization]] of the realm. Loop ends."
    ],
    end: true
  }
])

const present = computed(() => timeline.value.find(item => item.present))
const longRest = computed(() => timeline.value.find(item => item.longRest));
const currentTime = computed(() => present.value?.time)
const currentTimeOfDay = computed(() => getTimelineTimeOfDay(currentTime.value))
const currentTimeOfDayName = computed(() => getTimeOfDay(currentTimeOfDay.value))
const currentDay = computed(() => getTimelineDay(currentTime.value))

export {
  timelineLength,
  present,
  currentTime,
  loopNumber,
  getTimelineTimeOfDay,
  getTimelineDay,
  currentTimeOfDay,
  currentTimeOfDayName,
  currentDay,
  longRest
}

export default timeline;