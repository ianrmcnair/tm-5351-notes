import { ref, watch } from 'vue';

const views = {
  STARTUP: {
    name: 'startup',
    label: 'Startup',
    nav: false
  },
  TIMELINE: {
    name: 'timeline',
    label: 'Timeline'
  },
  NOTES: {
    name: 'codex',
    label: 'Codex'
  },
  CLOCK: {
    name: 'chronometer',
    label: 'Clock'
  },
  MUSIC: {
    name: 'music',
    label: 'Music'
  }
};

const viewKey = 'CURRENT_VIEW';
const skipStartupKey = 'SKIP_STARTUP';
const skipStartup = ref(localStorage.getItem(skipStartupKey) === 'true');

watch(skipStartup, () => {
  localStorage.setItem(skipStartupKey, skipStartup.value);
});

let defaultView = localStorage.getItem(viewKey);
if (defaultView && defaultView !== views.STARTUP.name) {
  defaultView = Object.values(views).find(view => view.name === defaultView);
}
if (!defaultView || defaultView === views.STARTUP.name) {
  defaultView = views.TIMELINE;
}

const currentView = ref(!skipStartup.value ? views.STARTUP : defaultView);
const isCurrentView = (view, current = currentView.value) => {
  return view.name === current.name;
}

watch(currentView, (newView) => {
  localStorage.setItem(viewKey, newView.name);
  window.scrollTo(0, 0)
});

export {
  isCurrentView,
  defaultView,
  currentView,
  skipStartup,
  views
}