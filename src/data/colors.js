export default {
  FUR: "var(--fur-color)",
  SHIRT: "var(--shirt-color)",
  CYBER: "var(--cyber-color)",
  TRIM: "var(--trim-color)",
  SCREEN: "var(--screen-color)",
  EMERALD: "var(--emerald-color)"
}