import { ref, onMounted } from 'vue'

export default function createScrollFade(elements) {
  const active = ref([]);

  onMounted(() => {
    const observer = new IntersectionObserver((entries) => {
      entries.forEach(entry => {
        active.value[elements.value.indexOf(entry.target)] = entry.isIntersecting;
      });
    }, {
      rootMargin: '0px',
      threshold: 0.25
    });

    elements.value.forEach((item) => {
      active.value.push(false);
      observer.observe(item);
    });
  });

  return active;
}