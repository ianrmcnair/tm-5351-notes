export default function (hour) {
  if (hour >= 12 && hour < 17) return 'afternoon';
  else if (hour >= 17 && hour < 20) return 'evening';
  else if (hour >= 20 || hour === 0) return 'night';
  else if (hour < 6) return 'early morning';
  return 'morning';
}