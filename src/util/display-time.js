export default hour => {
  const amPm = hour >= 12 ? 'pm' : 'am'
  let displayHour = hour;
  if (hour > 12) displayHour -= 12
  else if (hour === 0) displayHour = 12
  return `${displayHour}:00${amPm}`
}