export default function toAnchor(str) {
  return str.replace(/[^\w\s]/g, '').toLowerCase().replace(/\s/g, '-');
}